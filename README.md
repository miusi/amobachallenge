# Amoba Challenge
El proyecto fue realizado utilizando una arquitectura MVVM y diferentes frameworks detallados a continuación.`
Para ingresar se deben utilizar las siguientes credenciales:
User: admin@test.com
Password: 123456
Para mostrar los pacientes, estos poseén en la base de datos un campo medicId que corresponde al UID del usuario logueado.

# Frameworks utilizados
- Firebase Auth y Firestore
- Jetpack Navigation
- Hilt
- DataStore
- Maps
- Glide

## Mejoras restantes

- Si bien no se guarda el token al loguear, se guarda en dataStore el UID del usuario logueado.
- Falta parsear el mes/semana de nacimiento.
- Hacer zoom en el mapa donde se ubica el marker.

## Esquema de base de datos

![Database](https://i.ibb.co/VSwnXsF/Screenshot-2023-07-05-at-00-55-27.png)



