package com.miusi.amobapruebatecnica.repositories.auth

import com.google.firebase.auth.FirebaseUser
import com.miusi.amobapruebatecnica.model.Resource

interface AuthRepository {
    suspend fun login(email: String, password: String): Resource<FirebaseUser> // TODO: Refactor to response
    suspend fun logout()
    suspend fun getUserId(): String
}