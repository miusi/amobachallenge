package com.miusi.amobapruebatecnica.repositories

import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.stringPreferencesKey
import com.miusi.amobapruebatecnica.Config.AUTH_KEY
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map

class AuthDataStore constructor(private val dataStore: DataStore<Preferences>) {
    private val dataStoreKey = stringPreferencesKey(AUTH_KEY)

    suspend fun saveMedicId(id: String){
        dataStore.edit { pref ->
            pref[dataStoreKey] = id
        }
    }

    fun getData(): Flow<String> {
        return dataStore.data.map { pref ->
            if(pref[dataStoreKey] != null){
                pref[dataStoreKey] ?: ""
            } else {
                ""
            }
        }
    }

}