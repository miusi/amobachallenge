package com.miusi.amobapruebatecnica.repositories.auth

import com.miusi.amobapruebatecnica.model.AuthResponse
import com.miusi.amobapruebatecnica.model.Patient
import com.miusi.amobapruebatecnica.model.Resource


interface PatientRepository {
    suspend fun getClients(medicId: String): Resource<List<Patient>>
}