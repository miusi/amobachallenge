package com.miusi.amobapruebatecnica.repositories.auth

import android.util.Log
import await
import com.google.firebase.firestore.DocumentSnapshot
import com.google.firebase.firestore.FirebaseFirestore
import com.miusi.amobapruebatecnica.model.Patient
import com.miusi.amobapruebatecnica.model.PatientDAO
import com.miusi.amobapruebatecnica.model.Resource
import java.util.Date
import javax.inject.Inject

class PatientRepositoryImpl @Inject constructor(private val firestore: FirebaseFirestore) :
    PatientRepository {
    private var listOfPatients = mutableListOf<Patient>()

    override suspend fun getClients(medicId: String): Resource<List<Patient>> {
        val querySnapshot = firestore.collection("patient")
            .whereEqualTo("medicId", medicId)
            .get()
            .await()

        for (document in querySnapshot.documents) {
            snapshotToList(document)
        }
        return Resource.Success(listOfPatients)
    }

    private fun snapshotToList(
        snapshot: DocumentSnapshot,
    ) {
        val name = snapshot.getString("name") ?: ""
        val lastname = snapshot.getString("lastname") ?: ""
        val birthday = snapshot.getTimestamp("birthday")?.toDate() ?: Date()
        val ci = snapshot.getString("ci") ?: ""
        val email = snapshot.getString("email") ?: ""
        val genre = snapshot.getString("genre") ?: ""
        val telephone = snapshot.getString("telephone") ?: ""
        val address = snapshot.getString("address") ?: ""
        val latitude = snapshot.getString("latitude") ?: ""
        val longitude = snapshot.getString("longitude") ?: ""
        val image = snapshot.getString("imageUrl") ?: ""

        val patientDao = PatientDAO(
            name = name,
            lastname = lastname,
            birthday = birthday,
            address = address,
            ci = ci,
            email = email,
            genre = genre,
            telephone = telephone,
            latitude = latitude,
            longitude = longitude,
            image = image
        )

        listOfPatients.add(
            patientDao.toPatient()
        )
    }
}

