package com.miusi.amobapruebatecnica.presentation.detail.home

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.miusi.amobapruebatecnica.R
import com.miusi.amobapruebatecnica.databinding.ListPatientItemBinding
import com.miusi.amobapruebatecnica.model.Patient
import kotlinx.coroutines.withContext

class PatientAdapter(
    private val dataSet: List<Patient>,
    private val listener: OnClickListener
) : RecyclerView.Adapter<PatientAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.list_patient_item, parent, false)

        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return dataSet.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val patient = dataSet[position]
        with(holder){
            binding.listPatientNameText.text = patient.name
            binding.listPatientStatus.text = "Paciente actual"
            Glide
                .with(itemView)
                .load(patient.image)
                .circleCrop()
                .into(binding.itemPatientImage)
            setListener(patient)
        }
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val binding = ListPatientItemBinding.bind(view)

        fun setListener(patient: Patient){
            with(binding.root) {
                setOnClickListener { listener.onClick(patient) }
            }
        }
    }

}