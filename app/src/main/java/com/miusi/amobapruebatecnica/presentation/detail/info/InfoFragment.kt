package com.miusi.amobapruebatecnica.presentation.detail.info

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.View.OnClickListener
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.bumptech.glide.Glide
import com.miusi.amobapruebatecnica.R
import com.miusi.amobapruebatecnica.databinding.FragmentDetailBinding
import com.miusi.amobapruebatecnica.databinding.FragmentInfoBinding
import com.miusi.amobapruebatecnica.model.Patient
import dagger.hilt.android.AndroidEntryPoint
import java.lang.Exception
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.time.Period

@AndroidEntryPoint
class InfoFragment : Fragment(),
    com.miusi.amobapruebatecnica.presentation.detail.info.OnClickListener {

    private lateinit var binding: FragmentInfoBinding
    private val args by navArgs<InfoFragmentArgs>()
    private lateinit var currentPatient: Patient

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        binding = FragmentInfoBinding.inflate(inflater, container, false)
        currentPatient = args.currentPatient
        setTextFields()
        return binding.root
    }

    private fun setTextFields() {
        binding.listPatientNameText.text = "${currentPatient.name} {${currentPatient.lastname}"
        binding.infoId.text = currentPatient.ci
        binding.infoEmail.text = currentPatient.email
        binding.infoGenre.text = currentPatient.genre
        binding.infoTelephone.text = currentPatient.telephone
        binding.infoAddress.text = currentPatient.address

        Glide
            .with(requireActivity().applicationContext)
            .load(currentPatient.image)
            .circleCrop()
            .into(binding.itemPatientImage)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mapClickListener()
    }

    private fun mapClickListener() {
        binding.infoMap.setOnClickListener {
            OnClick(
                currentPatient.latitude!!,
                currentPatient.longitude!!
            )
        }
    }

    override fun OnClick(lat: Double, lon: Double) {
        val action = InfoFragmentDirections.actionInfoFragmentToMapFragment(
            lat.toFloat(), lon.toFloat() // Convert to float because of safeArgs
        )
        findNavController().navigate(action)
    }

}