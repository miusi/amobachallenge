package com.miusi.amobapruebatecnica.presentation.detail

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.miusi.amobapruebatecnica.model.Patient
import com.miusi.amobapruebatecnica.model.Resource
import com.miusi.amobapruebatecnica.repositories.AuthDataStore
import com.miusi.amobapruebatecnica.repositories.auth.PatientRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.launch
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import javax.inject.Inject

@HiltViewModel
class DetailViewModel @Inject constructor(
    private val patientRepository: PatientRepository,
    private val dataStore: AuthDataStore,
) : ViewModel() {

    private val _patientsFlow = MutableStateFlow<Resource<List<Patient>>?>(null)
    val patientsFlow: StateFlow<Resource<List<Patient>>?> = _patientsFlow

    suspend fun getClients() {
        _patientsFlow.value = Resource.Loading
        viewModelScope.launch {
            val medicId = dataStore.getData().first()
            _patientsFlow.value = patientRepository.getClients(medicId)
        }
    }

}


//    fun getClients(): List<Patient> {
//        return listOf(
//            Patient(
//                "Carlos",
//                "Moreno",
//                "30-40506070",
//                "carlos@moreno.com",
//                Date(),
//                "male",
//                "Humaita 1979",
//                -34.7751512,
//                -58.2711624,
//                "1154545454"
//            ),
//
//            )
//    }

fun formatDate(date: String): LocalDate {
    val formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy")
    return LocalDate.parse(date, formatter)
}


