package com.miusi.amobapruebatecnica.presentation.detail.home

import com.miusi.amobapruebatecnica.model.Patient

interface OnClickListener {
    fun onClick(patient: Patient)
    fun logout()
}