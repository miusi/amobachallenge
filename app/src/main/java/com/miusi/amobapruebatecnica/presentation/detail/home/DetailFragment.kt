package com.miusi.amobapruebatecnica.presentation.detail.home

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.miusi.amobapruebatecnica.R
import com.miusi.amobapruebatecnica.databinding.FragmentDetailBinding
import com.miusi.amobapruebatecnica.model.Patient
import com.miusi.amobapruebatecnica.model.Resource
import com.miusi.amobapruebatecnica.presentation.detail.DetailViewModel
import com.miusi.amobapruebatecnica.presentation.login.LoginActivity
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

@AndroidEntryPoint
class DetailFragment : Fragment(R.layout.fragment_detail), OnClickListener {

    private lateinit var binding: FragmentDetailBinding
    private lateinit var mAdapter: PatientAdapter
    private lateinit var mLinearLayout: LinearLayoutManager

    private val viewModel: DetailViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        binding = FragmentDetailBinding.inflate(inflater, container, false)

        CoroutineScope(Dispatchers.Main).launch {
            viewModel.getClients()
        }
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        observePatientFlow()
        binding.detailLogout.setOnClickListener {
            logout()
        }

    }

    private fun setupRecyclerView(list: List<Patient>) {
        mAdapter = PatientAdapter(list, this)
        mLinearLayout = LinearLayoutManager(context)

        binding.detailRvPatients.apply {
            setHasFixedSize(true)
            layoutManager = mLinearLayout
            adapter = mAdapter
        }
    }

    override fun onClick(patient: Patient) {
        val action =
            DetailFragmentDirections.actionDetailFragmentToInfoFragment(patient)
        findNavController().navigate(action)
    }

    override fun logout() {
        val intent = Intent(context, LoginActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        startActivity(intent)
        activity?.finish()
    }

    private fun observePatientFlow() {
        lifecycleScope.launch {
            viewModel.patientsFlow.collect() { status ->
                when (status) {
                    is Resource.Loading -> {
                        binding.detailProgressBar.show()
                    }
                    is Resource.Failure -> {
                        Toast.makeText(
                            context, "Error al loguear", Toast.LENGTH_SHORT
                        ).show()
                        binding.detailProgressBar.hide()
                    }

                    Resource.Loading -> null // Add loader
                    is Resource.Success -> {
                        setupRecyclerView(status.result)
                        binding.detailProgressBar.hide()
                    }

                    null -> return@collect
                }
            }
        }
    }

}