package com.miusi.amobapruebatecnica.presentation.detail.map

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.GoogleMap.OnMapClickListener
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.MarkerOptions
import com.google.type.LatLng
import com.miusi.amobapruebatecnica.R
import com.miusi.amobapruebatecnica.R.*
import com.miusi.amobapruebatecnica.presentation.detail.info.InfoFragmentArgs


class MapFragment : Fragment(), OnMapReadyCallback {

    private val args by navArgs<MapFragmentArgs>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        val view: View = inflater.inflate(layout.fragment_map, container, false)

        val mapFragment = childFragmentManager
            .findFragmentById(R.id.google_map) as? SupportMapFragment
        mapFragment?.getMapAsync(this)

        return view
    }

    override fun onMapReady(googleMap: GoogleMap) {
        val latitude = args.latitude.toDouble()
        val longitude = args.longitude.toDouble()

        val marker = com.google.android.gms.maps.model.LatLng(latitude, longitude)
        googleMap.addMarker(
            MarkerOptions()
                .position(marker)
                .title("Dirección del paciente")
        )
        googleMap.moveCamera(CameraUpdateFactory.newLatLng(marker))
    }
}