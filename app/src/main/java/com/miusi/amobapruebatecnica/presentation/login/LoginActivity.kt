package com.miusi.amobapruebatecnica.presentation.login

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import com.miusi.amobapruebatecnica.databinding.ActivityLoginBinding
import com.miusi.amobapruebatecnica.model.Resource
import com.miusi.amobapruebatecnica.presentation.detail.DetailActivity
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

@AndroidEntryPoint
class LoginActivity() : AppCompatActivity() {

    private lateinit var binding: ActivityLoginBinding
    private val viewModel: LoginViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)


        lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                viewModel.loginFlow.collect() { status ->
                    when (status) {
                        is Resource.Failure -> {
                            Toast.makeText(
                                applicationContext,
                                "Error al loguear",
                                Toast.LENGTH_SHORT
                            ).show()
                            binding.loginButton.isEnabled = true
                        }

                        Resource.Loading -> binding.loginButton.isEnabled = false
                        is Resource.Success -> {
                            val intent = Intent(applicationContext, DetailActivity::class.java)
                            startActivity(intent)
                        }

                        null -> return@collect
                    }
                }
            }
        }

        doLogin()
    }

    private fun doLogin() {
        binding.loginButton.setOnClickListener {
            CoroutineScope(Dispatchers.Main).launch {
                val user = binding.loginEmailInputText.text.toString()
                val password = binding.loginPasswordInputText.text.toString()
                viewModel.loginUser(user, password)
            }
        }

    }

}