package com.miusi.amobapruebatecnica.di

import com.google.firebase.auth.FirebaseAuth
import com.miusi.amobapruebatecnica.repositories.auth.AuthRepository
import com.miusi.amobapruebatecnica.repositories.auth.AuthFirebaseRepositoryImpl
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    @Provides
    fun provideFirebaseAuth(): FirebaseAuth = FirebaseAuth.getInstance()

    @Provides
    fun providesAuthRepository(impl: AuthFirebaseRepositoryImpl): AuthRepository = impl

}