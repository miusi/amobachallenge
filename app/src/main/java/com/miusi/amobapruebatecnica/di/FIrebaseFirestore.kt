package com.miusi.amobapruebatecnica.di

import com.google.firebase.firestore.FirebaseFirestore
import com.miusi.amobapruebatecnica.repositories.auth.PatientRepository
import com.miusi.amobapruebatecnica.repositories.auth.PatientRepositoryImpl
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
object FirebaseFirestore {

    @Provides
    fun provideFirebaseFirestore(): FirebaseFirestore = FirebaseFirestore.getInstance()

    @Provides
    fun providesPatientRepository(impl: PatientRepositoryImpl): PatientRepository = impl

}