package com.miusi.amobapruebatecnica.model

import com.google.gson.Gson

data class AuthResponse(
    val uid: String? = null
) {

    fun toJson(): String = Gson().toJson(this)

    companion object {
        fun fromJson(data: String): AuthResponse = Gson().fromJson(data, AuthResponse::class.java)
    }

}
