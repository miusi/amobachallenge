package com.miusi.amobapruebatecnica.model

data class Address(
    val latitude: String,
    val longitude: String,
) {
}