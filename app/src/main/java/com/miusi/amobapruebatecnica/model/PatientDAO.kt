package com.miusi.amobapruebatecnica.model

import java.util.Date

data class PatientDAO(
    val name: String,
    val lastname: String,
    val ci: String,
    val email: String,
    val birthday: Date,
    val genre: String,
    val address: String,
    val latitude: String,
    val longitude: String,
    val telephone: String,
    val image: String,
) {

    fun toPatient(): Patient{
        return Patient(
            name,
            lastname,
            ci,
            email,
            birthday,
            genre,
            address,
            latitude.toDouble(),
            longitude.toDouble(),
            telephone,
            image,
        )
    }

}