package com.miusi.amobapruebatecnica.model

import android.os.Parcelable
import java.util.Date

@kotlinx.parcelize.Parcelize
data class Patient(
    val name: String,
    val lastname: String,
    val ci: String,
    val email: String,
    val birthday: Date,
    val genre: String,
    val address: String,
    val latitude: Double,
    val longitude: Double,
    val telephone: String,
    val image: String
) : Parcelable